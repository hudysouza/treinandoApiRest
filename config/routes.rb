Rails.application.routes.draw do

  post '/api/v1/auth' => 'user_token#create'
  namespace :api do
    namespace :v1 do
      root 'home#index'
      get 'auth'  => 'home#auth'
      resources :people do
        resource :phones, only: %i[show create update destroy]
        resource :addresses, only: %i[show create update destroy]
        
      end

      #user actions
      get    '/users'          => 'users#index'
      get    '/users/current'  => 'users#current'
      post   '/users/create'   => 'users#create'
      patch  '/user/:id'       => 'users#update'
      delete '/user/:id'       => 'users#destroy'

    end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
