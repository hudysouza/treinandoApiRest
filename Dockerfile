# Dockerfile de criação de ambiente de desenvolvimento (sem a necessidade de usar vagrant)

FROM ruby:2.5.1

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs && apt-get install -y graphviz
RUN mkdir /Api

WORKDIR /Api

ADD Gemfile Gemfile.lock ./
RUN gem install rubocop

RUN bundle install
ADD . ./