class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :street_name
      t.string :street_number
      t.string :district
      t.string :city
      t.string :state
      t.string :zip_code
      t.references :person, foreign_key: true

      t.timestamps
    end
  end
end
