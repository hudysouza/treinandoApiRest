class PersonSerializer < ActiveModel::Serializer
  attributes :id, :name, :lastname, :birthdate, :cpf_num

  has_one :address
  has_many :phones
end
