class PhoneSerializer < ActiveModel::Serializer
  attributes :id, :number
  has_one :person
end
