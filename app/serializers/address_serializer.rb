class AddressSerializer < ActiveModel::Serializer
  attributes :id, :street_name, :street_number, :district, :city, :state, :zip_code
  has_one :person
end
