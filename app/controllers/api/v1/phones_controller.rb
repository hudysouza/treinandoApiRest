class Api::V1::PhonesController < ApplicationController
  before_action :set_person, only: [:create, :show, :update, :destroy]
  before_action :authenticate_user

  # GET /phones/1
  def show
    render json: @person.phones
  end

  # POST /phones
  def create
    @person.phones << Phone.new(phone_params)

    if @person.save
      render json: @person.phones, status: :created
    else
      render json: @person.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /phones/1
  def update
    phone = Phone.find(phone_params[:id])
    if phone.update(phone_params)
      render json: phone
    else
      render json: phone.errors, status: :unprocessable_entity
    end
  end

  # DELETE /phones/1
  def destroy
    phone = Phone.find(phone_params[:id])
    phone.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person
      @person = Person.find(params[:person_id])
    end

    # Only allow a trusted parameter "white list" through.
    def phone_params
      params.require(:phone).permit(:id, :number, :person_id)
    end
end
