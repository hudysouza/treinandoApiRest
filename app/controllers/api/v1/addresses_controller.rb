class Api::V1::AddressesController < ApplicationController
  before_action :set_person, only: [:show, :update, :destroy]
  before_action :authenticate_user

  # GET /addresses/1
  def show
    render json: @person.address
  end

  # POST /addresses
  def create
    @person.address << Address.new(address_params)

    if @person.save
      render json: @person.address, status: :created, location: @address
    else
      render json: @person.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /addresses/1
  def update
    address = Address.find(address_params[:id])
    if address.update(address_params)
      render json: address
    else
      render json: address.errors, status: :unprocessable_entity
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person
      @person = Person.find(params[:person_id])
    end

    # Only allow a trusted parameter "white list" through.
    def address_params
      params.require(:address).permit(:street_name, :street_number, :district, :city, :state, :zip_code, :person_id)
    end
end
