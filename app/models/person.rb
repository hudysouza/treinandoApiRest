class Person < ApplicationRecord
  has_many    :phones, dependent: :destroy
  has_one     :address, dependent: :destroy

  accepts_nested_attributes_for :address, allow_destroy: true, update_only: true
  accepts_nested_attributes_for :phones, allow_destroy: true
end
